#ifndef CVEXTRA_COMPONENTITERABLE_HPP_
#define CVEXTRA_COMPONENTITERABLE_HPP_


#include "core.hpp"
#include <opencv2/core/core.hpp>
#include <iterator>

namespace cvx {

namespace details {

class ComponentIterator : public std::iterator<std::input_iterator_tag, cvx::BinaryMat>
{
public:
    ComponentIterator(cv::Mat const& labels, int iLabel) : labels(labels), iLabel(iLabel){}

    auto operator ++() -> ComponentIterator&;
    auto operator ==(ComponentIterator const& other) const -> bool;
    auto operator !=(ComponentIterator const& other) const -> bool;
    auto operator  *() const -> cvx::BinaryMat;

    cv::Mat labels;
    int iLabel;

};

struct ComponentIterable
{
    auto begin() const -> ComponentIterator;
    auto end() const -> ComponentIterator;

    cv::Mat labels;
    int nLabels;
};

} // namespace details

auto connectedComponentMasks(
        cvx::BinaryMat const& mat,
        int connectivity,
        int ddepth = CV_32S
        ) -> details::ComponentIterable;

} // namespace cvx

#endif /* CVEXTRA_COMPONENTITERABLE_HPP_ */
