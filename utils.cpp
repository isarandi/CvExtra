#include "utils.hpp"
#include <stddef.h>
#include <ctime>
#include <string>

auto cvx::timestamp() -> std::string
{
    std::time_t rawtime;
    std::time(&rawtime);

    std::size_t const bufSize = 80;
    char buffer[bufSize];

    std::strftime(buffer, bufSize, "%Y_%m_%dT%H_%M_%S", std::localtime(&rawtime));
    return std::string(buffer);
}
