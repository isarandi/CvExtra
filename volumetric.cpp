#include "volumetric.hpp"
#include "improc.hpp"
#include "mats.hpp"
#include "ImageLoadingIterable.hpp"

using namespace std;
using namespace cv;
using namespace cvx;

auto cvx::
timeSlice(
        ImageLoadingIterable const& frames,
        LineSegment const& seg
        ) -> Mat
{
    cv::Mat result{frames.size(), seg.floorLength(), (*frames.begin()).type()};

    int iFrame=0;
    for (cv::Mat const& frame : frames)
    {
        cvx::lineProfile(frame, result.row(iFrame), seg);
        ++iFrame;
    }
    return result;
}


template <typename ImageSequence>
auto cvx::
timeSlice(
        ImageSequence frames,
        LineSegment const& seg
        ) -> Mat
{
    int nSamplesAlongLine = seg.floorLength();

    cv::Mat result{0,nSamplesAlongLine, (*frames.begin()).type()};
    for (cv::Mat const& frame : frames)
    {
        cvx::vconcat(result, cvxret::lineProfile(frame, seg), result);
    }
    return result;
}

// instantiate
#define INSTANTIATE_TIME_SLICE(type) \
    template \
    auto cvx::timeSlice<type>( \
            type frames, \
            LineSegment const& seg \
            ) -> Mat;

INSTANTIATE_TIME_SLICE(ImageLoadingIterable)
INSTANTIATE_TIME_SLICE(std::vector<cv::Mat>)
INSTANTIATE_TIME_SLICE(std::vector<cv::Mat3b>)
INSTANTIATE_TIME_SLICE(std::vector<cv::Mat1b>)
INSTANTIATE_TIME_SLICE(Iterable<typename ImageLoadingIterable::iterator_t>)
