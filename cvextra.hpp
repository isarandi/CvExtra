#ifndef CVEXTRA_HPP
#define CVEXTRA_HPP

#include "visualize.hpp"
#include "improc.hpp"
#include "math.hpp"
#include "mats.hpp"
#include "vectors.hpp"
#include "strings.hpp"
#include "io.hpp"
#include "gui.hpp"
#include "utils.hpp"
#include "filesystem.hpp"
#include "LoopRange.hpp"
#include "VideoIterable.hpp"
#include "ImageLoadingIterable.hpp"
#include "volumetric.hpp"
#include "colors.hpp"
#include "coords.hpp"
#include "shift.hpp"

#include "core.hpp"
#include "cvenums.hpp"
#include "cvret.hpp"

#include "cloning.hpp"
#include "stdx.hpp"

#endif // CVEXTRA_HPP
