#include "VideoIterable.hpp"

using namespace std;
using namespace cv;
using namespace cvx;

VideoIterator::
VideoIterator(VideoCapture* _videoCapture, bool end)
    : videoCapture(_videoCapture)
{
    if (end)
    {
        iFrame = VideoIterator::VIDEO_END;
    } else
    {
        *videoCapture >> m;
        iFrame = 0;
    }
}

auto VideoIterator::
operator *() -> Mat&
{
    return m;
}

auto VideoIterator::
operator ->() -> Mat*
{
    return &m;
}

auto VideoIterator::
operator ++() -> VideoIterator&
{
    *videoCapture >> m;
    iFrame = m.empty() ? VideoIterator::VIDEO_END : iFrame + 1;

    return *this;
}

auto VideoIterator::
operator ++(int) -> VideoIterator
{
    VideoIterator tmp{*this};
    ++(*this);
    return tmp;
}

auto VideoIterator::
operator +=(int incr) -> VideoIterator&
{
    for (int i=0; i < incr-1; i++)
    {
        videoCapture->grab();
    }
    return ++(*this);
}

auto VideoIterator::
operator ==(VideoIterator const& rhs) -> bool
{
    return iFrame == rhs.iFrame
            && videoCapture == rhs.videoCapture;
}
auto VideoIterator::
operator !=(VideoIterator const& rhs) -> bool
{
    return !(*this == rhs);
}

VideoIterable::
VideoIterable(VideoCapture const& vidCap)
    : vidCap(vidCap){}

VideoIterable::
VideoIterable(bpath const& filePath)
    : vidCap(filePath.string()){}

auto VideoIterable::
begin() -> VideoIterator
{
    return VideoIterator{&vidCap, false};
}

auto VideoIterable::
end() -> VideoIterator
{
    return VideoIterator{&vidCap, true};
}

auto cvx::
video(cvx::bpath const& filePath) -> VideoIterable
{
    return VideoIterable{filePath};
}

auto cvx::
video(VideoCapture const& vidCap) -> VideoIterable
{
    return VideoIterable{vidCap};
}
